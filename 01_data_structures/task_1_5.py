# -*- coding: utf-8 -*-
'''
Задание 1.5

Из строк pages1 и pages2 получить список страниц,
которые есть и в pages1  и в  pages2.

Результатом должен быть список: ['1', '3', '8']

Ограничение: Все задания надо выполнять используя только пройденные темы.

'''

import re


pages1 = 'интересные страницы: 1,2,3,5,8'
pages2 = 'интересные страницы: 1,3,8,9'


split1 = re.split(r':', pages1)
string1 = split1[1]
list1 = re.split(r',', string1)


split2 = re.split(r':', pages2)
string2 = split2[1]
list2 = re.split(r',', string2)


result =[]

for elem in list1:
    if elem in list2:
        result.append (elem)
            

print(result)
