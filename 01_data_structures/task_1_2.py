# -*- coding: utf-8 -*-
'''
Задание 1.2

Преобразовать строку mac адреса из формата XXXX:XXXX:XXXX в формат XXXX.XXXX.XXXX

Ограничение: Все задания надо выполнять используя только пройденные темы.

'''

import re
import string


mac = 'AAAA:BBBB:CCCC'

result = re.split(r':', mac)

dots = '.'


res=dots.join(result)

print(res)
